//Disini saya menggunakan operasi aritmatika untuk program yang akan saya buat, dengan tema sederhana
//berikut kode program yang saya buat beserta penjelasannya diatas kode tersebut

package com.company;
// Fungsi untuk mengaktifkan fungsi Scanner
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
	// Menginisialisasikan fungsi Scanner dengan nama variable “in”
    Scanner in=new Scanner(System.in);
    // Penginisialisasian variable.
    int a,b,jml,krg,kali,bagi,sisa;

        // Output program untuk memberikan isyarat agar pengguna memasukkan nilai
        System.out.print("Masukkan bilangan pertama : ");
        // Menggunakan fungsi Scanner untuk memasukkan nilai dan menyimpannya dalam variable yang telah ditentukan
        a=in.nextInt();
        // Output program untuk memberikan isyarat agar pengguna memasukkan nilai
        System.out.print("Masukkan bilangan kedua   : ");
        // Menggunakan fungsi Scanner untuk memasukkan nilai dan menyimpannya dalam variable yang telah ditentukan
        b=in.nextInt();

        // Proses program
        jml  = a + b;
        kali = a * b;
        bagi = a / b;
        sisa = a % b;
        krg  = a - b;

        // Output progrma dan hasil akhir program
        System.out.println(" ");
        System.out.println("Hasil operator aritmatika");
        System.out.println("=============================");
        System.out.println("Hasil dari penjumlahan : " + jml);
        System.out.println("Hasil dari perkalian   : " + kali);
        System.out.println("Hasil dari pembagian   : " + bagi);
        System.out.println("Hasil dari pengurangan : " + krg);
        System.out.println("Hasil dari sisa        : " + sisa);
    }
}
// Penjelasan algoritma program diatas
// 1. Pada awal program, memberikan fungsi untuk mengaktifkan fungsi Scanner
// 2. Selanjutnya, kita harus memberikan nama file dan memberikan fungsi public static void main
// 3. Lalu, menginisialisasikan fungsi Scanner dengan nama variable “in” agar kita dapat menginputkan nilai dari keyboard
// 4. Selanjutnya, menginisialisasikan variable yang dibutuhkan
// 5. Kemudian, kita mengeluarkan karakter yang digunakan sebagai perintah untuk memasukkan nilai secara manual melalui keyboard
// 6. Kemudian kita harus menginisialisasi fungsi Scanner untuk memasukkan nilai dan menyimpannya dalam variable yang telah ditentukan sehingga kita bisa menginputkan nilai dan bisa menyimpannya.
// 7. Membuat proses atau fungsi aritmatika
// 8. Mencetak program, jika yang dikeluarkan adalah karakter maka cara penulisannya adalah karakter yang akan dikeluarkan harus berada didala tanda petik dua, jika bukan karakter maka harus berada diluar tanda petik dua.