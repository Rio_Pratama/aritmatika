//Disini saya menggunakan operasi aritmatika untuk program yang akan saya buat, dengan tema sederhana
//berikut kode program yang saya buat beserta penjelasannya dibawah kode tersebut

package com.company;

//import java.util.Scanner memiliki fungsi untuk mengaktifkan fungsi Scanner

import java.util.Scanner;
public class main {

    public static void main (String[] args) {

        //Scanner in = new Scanner(System.in) memiliki fungsi untuk menginisialisasikan fungsi Scanner dengan nama variabel "in"
        Scanner in = new Scanner(System.in);
        //penginisialisasian variabel
        int A, B, jumlah, kurang, kali, bagi, sisa;

        //output program untuk memberikan isyarat agar pengguna memasukkan nilai
        System.out.println("Bilangan 1: ");
        //menggunakan fungsi Scanner untuk memasukkan nilai dan menyimpannya dalam variabel yang telah ditentukan
        A = in.nextInt();

        //output program untuk memberikan isyarat agar pengguna memasukkan nilai
        System.out.println("Bilangan 2: ");
        //menggunakan fungsi Scanner untuk memasukkan nilai dan menyimpannya dalam variabel yang telah ditentukan
        B = in.nextInt();

        //proses program yang sudah kita run dan kita masukkan bilangan yang ingin kita jadikan sebuah nilai
        jumlah = A + B;
        kurang = A - B;
        kali = A * B;
        bagi = A / B;
        sisa = A % B;

        //Output program serta hasil program dari program aplikasi aritmatika sederhana saya ini
        System.out.println(" ");
        System.out.println("Hasil Operator Aritmatika");
        System.out.println("===========================");
        System.out.println("Jumlah=" + jumlah);
        System.out.println("Kurang=" + kurang);
        System.out.println("Kali=" + kali);
        System.out.println("Bagi=" + bagi);
        System.out.println("Sisa=" + sisa);
    }
}
